import 'dart:convert';

import 'package:http/http.dart';

import '../state/settings.dart';

class Etablissement {
  ///[nom] de l'[Etablissement]
  String nom;

  ///[url] d'accès à l'API de l'[Etablissement]
  String url;

  ///[logo] associé à l'[Etablissement]
  String logo;

  ///[bool] indiquant si l'[Etablissement] est activé
  bool xflop;

  Etablissement({this.nom, this.url, this.logo, this.xflop});

  ///Constructeur Factory depuis un JSON
  factory Etablissement.fromJSON(Map<String, dynamic> json) => Etablissement(
        nom: json['name'] ?? (json['nom'] ?? null),
        url: json['flop_url'] ?? (json['url'] ?? null),
        xflop: json['xflop'] ?? false,
        logo: json['logo'] ?? 'null',
      );

  ///Crée une [Map] à partir de l'objet.
  Map<String, dynamic> get toMap => {
        'nom': this.nom,
        'url': this.url,
        'xflop': this.xflop,
        'logo': this.logo,
      };

  ///Retourne une chaîne JSON de l'objet.
  String get toJSON => jsonEncode(this.toMap);

  ///Méthode d'affichage de [Etablissement]
  @override
  String toString() {
    return this.toJSON;
  }

  ///Méthode statique créant une liste de [Etablissement] à partir d'une réponse HTTP.
  static Future<List<Etablissement>> createListFromResponse(
      Response response) async {
    var etablissements = jsonDecode(utf8.decode(response.bodyBytes));
    var res = <Etablissement>[];
    for (var eta in etablissements) {
      res.add(Etablissement(
        nom: eta['name'],
        url: eta['flop_url'],
        xflop: eta['xflop'],
        logo: eta['logo']['url'],
      ));
    }
    return res;
  }

  ///Methode de setting d'[Etablissement] dans les [Settings]
  Future<void> setEtablissements(Etablissement etablissement) async {
    Settings settings = await Settings.getConfiguration();
    settings.etablissement = etablissement;
  }
}
