import 'package:flop_edt_app/models/state/app_state.dart';

class AppException implements Exception {
  final _message;
  final _prefix;

  AppException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException(String message, AppState as) {
    as.msgLoading = message;
  }
}
