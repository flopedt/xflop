import 'package:flop_edt_app/models/state/app_state.dart';
import 'package:flop_edt_app/models/state/settings.dart';
import 'package:flop_edt_app/state_manager/state_widget.dart';
import 'package:flop_edt_app/utils/constants.dart';
import 'package:flop_edt_app/views/login/login_screen.dart';
import 'package:flutter/material.dart';

import 'components/etablissement_chooser.dart';

class CreateSettingsScreen extends StatefulWidget {
  @override
  _CreateSettingsScreenState createState() => _CreateSettingsScreenState();
}

class _CreateSettingsScreenState extends State<CreateSettingsScreen> {
  AppState state;

  ScrollController _scrollController = new ScrollController();
  bool isProfSelected;
  bool etablissementSelected;
  Settings settings;

  @override
  void initState() {
    super.initState();
    isProfSelected = false;
    etablissementSelected = false;
  }

  void handleSettingsReceived(dynamic value) {
    setState(() {
      settings = value;
    });
  }

  void saveSettings() => StateWidget.of(context).setSettings(settings);

  void handleSelect() {
    var theme = Theme.of(context);
    state = StateWidget.of(context).state;
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
        ),
        context: context,
        builder: (context) {
          return Container(
              decoration: BoxDecoration(
                color: theme.scaffoldBackgroundColor,
                borderRadius: BorderRadius.all(Radius.circular(25.0)),
              ),
              child: SingleChildScrollView(
                padding: EdgeInsets.all(5),
                child: Column(
                  children: <Widget>[
                    EtablissementSelector(
                      settings: state.settings,
                      onSelect: (value) {
                        setState(() {
                          settings = value;
                          etablissementSelected = true;
                        });
                        settings.saveConfiguration();
                        StateWidget.of(context).setSettings(settings);
                        //StateWidget.of(context).initData2();
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ));
        });
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    state = StateWidget.of(context).state;
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: SingleChildScrollView(
            controller: _scrollController,
            child: SafeArea(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          Constants.logoPath,
                          width: 100,
                        ),
                        Text(
                          'xFlop!',
                          style: theme.textTheme.headline4.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      'Afin d\'avoir accès à votre emploi du temps, veuillez sélectionner votre établissement et vous connecter avec vos identifiants.',
                      style: theme.textTheme.bodyText1,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    _etablissementButton(theme),
                    SizedBox(
                      height: 30,
                    ),
                    state.etablissement != null
                        ? _loginButton(theme)
                        : Container(),
                    // etablissementSelected || state.departments.isNotEmpty
                    //     ? Row(
                    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //         children: <Widget>[
                    //           Row(
                    //             children: [
                    //               Icon(
                    //                   const IconData(57583,
                    //                       fontFamily: 'MaterialIcons'),
                    //                   color: theme.iconTheme.color),
                    //               SizedBox(
                    //                 width: 10,
                    //               ),
                    //               Text(
                    //                 'Mode professeur',
                    //                 style: theme.textTheme.bodyText1,
                    //               ),
                    //             ],
                    //           ),
                    //           Switch.adaptive(
                    //             value: isProfSelected,
                    //             onChanged: (bool newValue) {
                    //               setState(() {
                    //                 isProfSelected = newValue;
                    //                 _scrollController.animateTo(180.0,
                    //                     duration: Duration(milliseconds: 500),
                    //                     curve: Curves.ease);
                    //               });
                    //             },
                    //           )
                    //         ],
                    //       )
                    //     : Container(),
                    // etablissementSelected || state.departments.isNotEmpty
                    //     ? isProfSelected
                    //         ? TutorSettingsSelector(
                    //             onSelected: handleSettingsReceived,
                    //           )
                    //         : StudentSettingsSelector(
                    //             onSelected: handleSettingsReceived,
                    //           )
                    //     : Container(),
                    // SizedBox(
                    //   height: 30,
                    // ),
                    // _validateButton(theme),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget _etablissementButton(ThemeData theme) => ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
          backgroundColor: Color(0xFFFF6C00),
          padding: EdgeInsets.all(10),
          shadowColor: Color(0xFFFF6C00),
          elevation: 5,
        ),
        onPressed: handleSelect,
        child: Text(state.etablissement == null
            ? 'Etablissement'
            : state.etablissement.nom),
      );

  Widget _loginButton(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton.icon(
          style: theme.elevatedButtonTheme.style,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginScreen()),
            );
          },
          icon: Icon(const IconData(63626, fontFamily: 'MaterialIcons')),
          label: Text(
            'Se connecter',
            style: theme.textTheme.button,
          ),
        ),
      );

  Widget _validateButton(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          style: theme.elevatedButtonTheme.style,
          onPressed: settings == null
              ? null
              : () {
                  saveSettings();
                },
          child: Text(
            'Valider',
            style: theme.textTheme.button,
          ),
        ),
      );
}
