import 'package:flop_edt_app/api/api_provider.dart';
import 'package:flop_edt_app/models/state/app_state.dart';
import 'package:flop_edt_app/models/state/settings.dart';
import 'package:flop_edt_app/state_manager/state_widget.dart';
import 'package:flop_edt_app/views/settings/components/change_password_screen.dart';
import 'package:flutter/material.dart';

import '../../../models/resources/user.dart';
import '../../divers/contact_screen.dart';
import '../create_settings_screen.dart';

class UserSettings extends StatefulWidget {
  final ValueChanged onSelected;
  final Settings settings;
  final User user;

  const UserSettings({Key key, this.onSelected, this.settings, this.user})
      : super(key: key);
  @override
  _StudentSettingsSelectorState createState() =>
      _StudentSettingsSelectorState();
}

class _StudentSettingsSelectorState extends State<UserSettings> {
  AppState state;

  User user;

  @override
  void initState() {
    super.initState();
    user = widget.user;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData theme = Theme.of(context);
    state = StateWidget.of(context).state;
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Center(
                  child: Text(
                user.firstName + ' ' + user.lastName.toUpperCase(),
                style: theme.textTheme.headline4.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              )),
              SizedBox(
                height: 10,
              ),
              Card(
                color: theme.cardTheme.color,
                elevation: theme.cardTheme.elevation,
                shape: theme.cardTheme.shape,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Color(0xFFFFFFFF),
                        child: Icon(
                          const IconData(0xf816, fontFamily: 'MaterialIcons'),
                          color: Colors.black,
                        ),
                      ),
                      title: Text(user.username,
                          style: TextStyle(
                            color: Colors.white,
                          )),
                      subtitle: Text(user.email,
                          style: TextStyle(
                            color: Colors.white54,
                          )),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Card(
                color: theme.cardTheme.color,
                elevation: theme.cardTheme.elevation,
                shape: theme.cardTheme.shape,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Color(0xFFFFFFFF),
                        child: Icon(
                          const IconData(0xf012e, fontFamily: 'MaterialIcons'),
                          color: Colors.black,
                        ),
                      ),
                      title: Text(
                          user.isStudent
                              ? user.departement +
                                  ' - ' +
                                  user.promo +
                                  ' - ' +
                                  user.groupe
                              : user.isTutor
                                  ? user.departement
                                  : "Inconnu",
                          style: TextStyle(
                            color: Colors.white,
                          )),
                      subtitle: Text(this.state.settings.etablissement.nom,
                          style: TextStyle(
                            color: Colors.white54,
                          )),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              _editPassword(theme),
              SizedBox(
                height: 20,
              ),
              _logout(theme),
            ],
          ),
        ),
      ],
    );
  }

  Widget _editPassword(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton.icon(
          style: theme.elevatedButtonTheme.style,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ChangePasswordScreen()),
            );
          },
          icon: Icon(const IconData(0xf0050, fontFamily: 'MaterialIcons')),
          label: Text(
            'Changer le mot de passe',
            style: theme.textTheme.button,
          ),
        ),
      );

  /// En attente d'une fonction dédié au changement d'e-mail
  Widget _editEmail(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton.icon(
          style: theme.elevatedButtonTheme.style,
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ContactScreen()),
            );
          },
          icon: Icon(const IconData(0xf705, fontFamily: 'MaterialIcons')),
          label: Text(
            'Changer l\'adresse e-mail',
            style: theme.textTheme.button,
          ),
        ),
      );

  Widget _logout(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton.icon(
          style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.red)),
          onPressed: () async {
            APIProvider api = APIProvider();
            await api.getLogout(user.token);
            state = StateWidget.of(context).state;
            Settings settings = state.settings;
            settings.user = null;
            StateWidget.of(context).setSettings(settings);
            Navigator.of(context).pop();
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateSettingsScreen()),
            );
          },
          icon: Icon(const IconData(0xf88b, fontFamily: 'MaterialIcons')),
          label: Text(
            'Se déconnecter',
            style: theme.textTheme.button,
          ),
        ),
      );
}
