import 'package:flop_edt_app/models/state/app_state.dart';
import 'package:flop_edt_app/state_manager/state_widget.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../../models/resources/user.dart';
import '../../../models/state/settings.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  AppState state;
  bool passwordNoEmpty2 = false;
  TextEditingController passwordController2 = new TextEditingController();
  bool passwordNoEmpty3 = false;
  TextEditingController passwordController3 = new TextEditingController();
  String messageError = '';
  bool buttonActivated = false;
  @override
  void initState() {
    super.initState();
  }

  send() async {
    try {
      await User.postChangePassword(state.settings.user.token,
          passwordController2.text, passwordController3.text);
      Settings settings = await Settings.getConfiguration();
      settings.user.password = passwordController2.text;
      settings.saveConfiguration();
      Navigator.of(context, rootNavigator: true).pop();
      return Fluttertoast.showToast(
          msg: "Le mot de passe à bien été modifié",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Color(0xFF69F0AE),
          textColor: Color(0xFF155032),
          fontSize: 16.0);
    } catch (e) {
      setState(() {
        messageError = e.toString();
      });
    }
  }

  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    state = StateWidget.of(context).state;
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      GestureDetector(
                        child: Icon(const IconData(0xf570,
                            fontFamily: 'MaterialIcons')),
                        onTap: () =>
                            Navigator.of(context, rootNavigator: true).pop(),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Mot de passe',
                        style: theme.textTheme.headline4.copyWith(
                          fontWeight: FontWeight.bold,
                          fontSize: 25,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Center(
                    child: Text(
                      'Mettez à jour votre mot de passe',
                      style: theme.textTheme.headline4.copyWith(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text(
                      'Votre nouveau mot de passe doit avoir une longueur d\'au moins 8 caractères.',
                      textAlign: TextAlign.center,
                      style: theme.textTheme.headline4.copyWith(
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _messageError(theme),
                  SizedBox(
                    height: 5,
                  ),
                  _newPasswordTextfield(theme),
                  _newPasswordTextfield2(theme),
                  SizedBox(
                    height: 20,
                  ),
                  _sendButton(theme),
                ]),
          ),
        ),
      ),
    );
  }

  Widget _messageError(ThemeData theme) => Center(
      child: Text(messageError,
          textAlign: TextAlign.center, style: TextStyle(color: Colors.red)));

  Widget _newPasswordTextfield(ThemeData theme) => Padding(
      padding:
          const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5.0,
              color: Colors.black.withOpacity(0.25),
            ),
          ],
        ),
        child: TextField(
          onSubmitted: (value) {
            FocusManager.instance.primaryFocus?.unfocus();
            send();
          },
          controller: passwordController2,
          obscureText: true,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            hintText: 'Enter le nouveau mot de passe',
            hintStyle: TextStyle(
                color: (theme.iconTheme.color == Colors.white)
                    ? Colors.grey.shade500
                    : theme.primaryColor),
            filled: true,
            fillColor: (theme.iconTheme.color == Colors.white)
                ? theme.primaryColor
                : Colors.white,
          ),
          style: theme.textTheme.bodyText1,
          onChanged: (value) {
            passwordController2.text != ""
                ? setState(() {
                    passwordNoEmpty2 = true;
                    messageError = '';
                  })
                : setState(() {
                    passwordNoEmpty2 = false;
                    messageError = '';
                  });
          },
        ),
      ));

  Widget _newPasswordTextfield2(ThemeData theme) => Padding(
      padding:
          const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5.0,
              color: Colors.black.withOpacity(0.25),
            ),
          ],
        ),
        child: TextField(
          onSubmitted: (value) {
            FocusManager.instance.primaryFocus?.unfocus();
            send();
          },
          controller: passwordController3,
          obscureText: true,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            hintText: 'Enter le nouveau mot de passe',
            hintStyle: TextStyle(
                color: (theme.iconTheme.color == Colors.white)
                    ? Colors.grey.shade500
                    : theme.primaryColor),
            filled: true,
            fillColor: (theme.iconTheme.color == Colors.white)
                ? theme.primaryColor
                : Colors.white,
          ),
          style: theme.textTheme.bodyText1,
          onChanged: (value) {
            passwordController3.text != ""
                ? setState(() {
                    passwordNoEmpty3 = true;
                    messageError = '';
                  })
                : setState(() {
                    passwordNoEmpty3 = false;
                    messageError = '';
                  });
          },
        ),
      ));

  Widget _sendButton(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.all(15),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: theme.primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            padding: EdgeInsets.all(10),
          ),
          onPressed: !(passwordNoEmpty2 & passwordNoEmpty3)
              ? null
              : () async {
                  send();
                },
          child: Text(
            'Enregistrer',
            style: theme.textTheme.button,
          ),
        ),
      );
}
