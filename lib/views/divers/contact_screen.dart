import 'package:flop_edt_app/models/state/app_state.dart';
import 'package:flop_edt_app/state_manager/state_widget.dart';
import 'package:flop_edt_app/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:image_picker/image_picker.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  AppState state;
  bool buttonActivated = false;
  TextEditingController emailController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  List<String> attachments = [];
  bool isHTML = false;

  final _recipientController = TextEditingController(
    text: 'contact@flopedt.org',
  );

  final _subjectController = TextEditingController();

  final _bodyController = TextEditingController();

  Future<void> send() async {
    final Email email = Email(
      body: _bodyController.text,
      subject: _subjectController.text,
      recipients: [_recipientController.text],
      attachmentPaths: attachments,
    );

    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
    } catch (error) {
      platformResponse = error.toString();
    }

    if (!mounted) return;

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(platformResponse),
      ),
    );
  }

  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    state = StateWidget.of(context).state;
    return Scaffold(
        body: SingleChildScrollView(
            child: SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                GestureDetector(
                  child:
                      Icon(const IconData(0xf570, fontFamily: 'MaterialIcons')),
                  onTap: () => Navigator.of(context, rootNavigator: true).pop(),
                ),
                SizedBox(
                  width: 30,
                ),
                Text(
                  'Contact',
                  style: theme.textTheme.headline4.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    Constants.logoPath,
                    width: 100,
                  ),
                  Text(
                    'xFlop!',
                    style: theme.textTheme.headline4.copyWith(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            _subjectTextfield(theme),
            _bodyTextfield(theme),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  for (var i = 0; i < attachments.length; i++)
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            attachments[i],
                            softWrap: false,
                            overflow: TextOverflow.fade,
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.remove_circle),
                          onPressed: () => {_removeAttachment(i)},
                        )
                      ],
                    ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: IconButton(
                      icon: Icon(Icons.attach_file),
                      onPressed: _openImagePicker,
                    ),
                  ),
                  _sendButton(theme),
                ],
              ),
            ),
          ],
        ),
      ),
    )));
  }

  Widget _subjectTextfield(ThemeData theme) => Padding(
      padding:
          const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5.0,
              color: Colors.black.withOpacity(0.25),
            ),
          ],
        ),
        child: TextField(
          controller: _subjectController,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            hintText: 'Entrez un sujet',
            hintStyle: TextStyle(
                color: (theme.iconTheme.color == Colors.white)
                    ? Colors.grey.shade500
                    : theme.primaryColor),
            filled: true,
            fillColor: (theme.iconTheme.color == Colors.white)
                ? theme.primaryColor
                : Colors.white,
          ),
          style: theme.textTheme.bodyText1,
          // onChanged: (value) {
          //   if (userController.text != "") {
          //     setState(() {
          //       userNoEmpty = true;
          //     });
          //   } else {
          //     setState(() {
          //       userNoEmpty = false;
          //     });
          //   }
          // },
        ),
      ));

  Widget _bodyTextfield(ThemeData theme) => Padding(
      padding:
          const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 30),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5.0,
              color: Colors.black.withOpacity(0.25),
            ),
          ],
        ),
        child: TextField(
          controller: _bodyController,
          textAlignVertical: TextAlignVertical.top,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            hintText: 'Entrez un message',
            hintStyle: TextStyle(
                color: (theme.iconTheme.color == Colors.white)
                    ? Colors.grey.shade500
                    : theme.primaryColor),
            filled: true,
            fillColor: (theme.iconTheme.color == Colors.white)
                ? theme.primaryColor
                : Colors.white,
          ),
          style: theme.textTheme.bodyText1,
        ),
      ));
  Widget _sendButton(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: theme.primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            padding: EdgeInsets.all(10),
          ),
          onPressed: send,
          child: Text(
            'Envoyer',
            style: theme.textTheme.button,
          ),
        ),
      );

  void _openImagePicker() async {
    final ImagePicker _picker = ImagePicker();
    final XFile image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      setState(() {
        attachments.add(image.path);
      });
    }
  }

  void _removeAttachment(int index) {
    setState(() {
      attachments.removeAt(index);
    });
  }
}
