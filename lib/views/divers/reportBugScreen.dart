import 'package:flop_edt_app/api/api_provider.dart';
import 'package:flop_edt_app/models/resources/Incident.dart';
import 'package:flop_edt_app/models/state/app_state.dart';
import 'package:flop_edt_app/state_manager/state_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ReportBugScreen extends StatefulWidget {
  @override
  _ReportBugScreenState createState() => _ReportBugScreenState();
}

class _ReportBugScreenState extends State<ReportBugScreen> {
  AppState state;
  bool buttonActivated = false;
  bool isLoading = false;
  TextEditingController emailController = new TextEditingController();
  String messageError = '';

  @override
  void initState() {
    super.initState();
    listIncidents();
  }

  var isDark;
  bool isHTML = false;

  final _titreController = TextEditingController();

  final _descriptionController = TextEditingController();

  List<Incident> incident = [];

  listIncidents() async {
    setState(() {
      isLoading = true;
    });
    APIProvider api = new APIProvider();
    List<Incident> inc = await api.listIncidents();
    setState(() {
      incident = inc;
      isLoading = false;
    });
  }

  send(String title, String description) async {
    APIProvider api = new APIProvider();
    var msg = await api.createIssue(title, description);
    if (msg) {
      Fluttertoast.showToast(
          msg: "Le signalement a bien été éffectué",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Color(0xFF69F0AE),
          textColor: Color(0xFF155032),
          fontSize: 16.0);
      listIncidents();
    } else {
      setState(() {
        messageError = 'Une erreur s\'est produite';
      });
    }
  }

  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    state = StateWidget.of(context).state;
    isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  GestureDetector(
                    child: Icon(
                        const IconData(0xf570, fontFamily: 'MaterialIcons')),
                    onTap: () =>
                        Navigator.of(context, rootNavigator: true).pop(),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    'Bug',
                    style: theme.textTheme.headline4.copyWith(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Center(
                child: Text(
                  'Signaler un bug',
                  style: theme.textTheme.headline4.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  'Vous avez observé un bug ?\nMerci de le décrire ci-dessous :',
                  textAlign: TextAlign.center,
                  style: theme.textTheme.headline4.copyWith(
                    fontWeight: FontWeight.normal,
                    fontSize: 15,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              _messageError(theme),
              SizedBox(
                height: 5,
              ),
              _titreTextfield(theme),
              _descriptionTextfield(theme),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    _sendButton(theme),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  'Consultez d\'abord la liste avant de signaler un bug.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.grey.shade400,
                    fontSize: 12,
                  ),
                ),
              ),
              _listIncidents(theme),
            ],
          ),
        ),
      ),
    );
  }

  Widget _messageError(ThemeData theme) => Center(
      child: Text(messageError,
          textAlign: TextAlign.center, style: TextStyle(color: Colors.red)));

  Widget _listIncidents(ThemeData theme) => Expanded(
        //height: 100,
        child: isLoading
            ? SpinKitFadingFour(
                color: Colors.white,
              )
            : incident.isEmpty
                ? Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Center(
                        child: Text(
                          'Aucun bug signalé.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.grey.shade400,
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  )
                : ListView.builder(
                    itemCount: incident.length,
                    itemBuilder: (context, index) {
                      Incident incid = incident[index];
                      return Padding(
                          padding: EdgeInsets.all(10),
                          child: AnimatedContainer(
                              duration: Duration(milliseconds: 500),
                              curve: Curves.easeInOut,
                              margin: EdgeInsets.symmetric(horizontal: 5),
                              padding: EdgeInsets.symmetric(horizontal: 30),
                              decoration: BoxDecoration(
                                color: Color(0xFFBD0026),
                                borderRadius: BorderRadius.circular(50),
                                boxShadow: [
                                  isDark
                                      ? BoxShadow(
                                          color: Color(0xFFBD0026)
                                              .withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0, 3),
                                        )
                                      : BoxShadow(),
                                ],
                              ),
                              child: Center(
                                  child: ListTile(
                                onTap: () => incid.displayInformations(context),
                                title: Text(
                                  '${incid.titre}',
                                  style: theme.textTheme.button,
                                ),
                              ))));
                    },
                  ),
      );

  Widget _titreTextfield(ThemeData theme) => Padding(
      padding:
          const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5.0,
              color: Colors.black.withOpacity(0.25),
            ),
          ],
        ),
        child: TextField(
          onSubmitted: (value) {
            FocusManager.instance.primaryFocus?.unfocus();
            send(_titreController.text, _descriptionController.text);
          },
          controller: _titreController,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            hintText: 'Entrez un titre',
            hintStyle: TextStyle(
                color: (theme.iconTheme.color == Colors.white)
                    ? Colors.grey.shade500
                    : theme.primaryColor),
            filled: true,
            fillColor: (theme.iconTheme.color == Colors.white)
                ? theme.primaryColor
                : Colors.white,
          ),
          style: theme.textTheme.bodyText1,
        ),
      ));

  Widget _descriptionTextfield(ThemeData theme) => Padding(
      padding:
          const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 30),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 5.0,
              color: Colors.black.withOpacity(0.25),
            ),
          ],
        ),
        child: TextField(
          onSubmitted: (value) {
            FocusManager.instance.primaryFocus?.unfocus();
            send(_titreController.text, _descriptionController.text);
          },
          controller: _descriptionController,
          textAlignVertical: TextAlignVertical.top,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.transparent),
                borderRadius: BorderRadius.all(Radius.circular(30))),
            hintText: 'Entrez une description',
            hintStyle: TextStyle(
                color: (theme.iconTheme.color == Colors.white)
                    ? Colors.grey.shade500
                    : theme.primaryColor),
            filled: true,
            fillColor: (theme.iconTheme.color == Colors.white)
                ? theme.primaryColor
                : Colors.white,
          ),
          style: theme.textTheme.bodyText1,
        ),
      ));

  Widget _sendButton(ThemeData theme) => Container(
        padding: EdgeInsets.all(5),
        width: MediaQuery.of(context).size.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: theme.primaryColor,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
            padding: EdgeInsets.all(10),
          ),
          onPressed: () {
            FocusManager.instance.primaryFocus?.unfocus();
            send(_titreController.text, _descriptionController.text);
          },
          child: Text(
            'Envoyer',
            style: theme.textTheme.button,
          ),
        ),
      );
}
